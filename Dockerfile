FROM node:boron

MAINTAINER Reekoh

RUN apt-get update && apt-get install -y build-essential

RUN mkdir -p /home/node/app
COPY . /home/node/app

WORKDIR /home/node/app

# Install dependencies
RUN npm install pm2 -g
RUN npm install

CMD ["pm2-docker", "--json", "app.yml"]